﻿using Prism.Events;
using ThumbnailSample.ViewModels;

namespace ThumbnailSample.Events
{
    public class SlideSelectedEvent : PubSubEvent<SlideViewModel>
    {

    }
}
