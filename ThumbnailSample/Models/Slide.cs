﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbnailSample.Models
{
    [Serializable]
    public class Slide : ModelBase
    {
        public SolidColorBrush Background { get; set; }
        public List<Shape> Shapes { get; set; }
    }
}
