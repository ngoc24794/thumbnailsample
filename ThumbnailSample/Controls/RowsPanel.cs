﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace ThumbnailSample.Controls
{
    public class RowsPanel : Panel, IScrollInfo
    {
        public RowsPanel()
        {
            _trans = new TranslateTransform();
            RenderTransform = _trans;
        }

        private bool _loaded = false;

        /// <summary>
        /// Thuộc tính LazySlidesCount
        /// </summary>
        public int LazySlidesCount
        {
            get { return (int)GetValue(LazySlidesCountProperty); }
            set { SetValue(LazySlidesCountProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính LazySlidesCount.
        /// </summary>
        public static readonly DependencyProperty LazySlidesCountProperty =
            DependencyProperty.Register("LazySlidesCount", typeof(int), typeof(RowsPanel), new PropertyMetadata(0));

        
        public bool AnimateScroll { get; set; } = true;
        public double RowHeight { get; set; } = 30;
        protected override Size MeasureOverride(Size availableSize)
        {
            UpdateScrollInfo(availableSize);
            foreach (UIElement child in Children)
            {
                child.Measure(availableSize);
            }
            return availableSize;
        }

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            if (Children.Count > 0)
            {
                double arrangeHeight = Children[0].DesiredSize.Height;
                if (Children.Count > 1)
                {
                    for (int i = 1; i < Children.Count; i++)
                    {
                        arrangeHeight = Math.Min(Children[i].DesiredSize.Height, arrangeHeight);
                    }
                }
                RowHeight = arrangeHeight;
                UpdateScrollInfo(arrangeSize);
                double previousChildSize = 0;
                List<int> selectedIndexes = new List<int>();
                System.Collections.IList list = Children;
                for (int i = 0; i < list.Count; i++)
                {
                    UIElement child = (UIElement)list[i];
                    SetZIndex(child, i);
                    Rect rect = new Rect(0, previousChildSize, arrangeSize.Width, arrangeHeight);
                    child.Arrange(rect);
                    previousChildSize += arrangeHeight;
                    if ((child as ListBoxItem)?.IsSelected == true)
                    {
                        selectedIndexes.Add(i);
                    }
                }
                if (selectedIndexes?.Count > 0)
                {
                    int lastIndex = selectedIndexes.LastOrDefault();
                    int lastViewportIndex = (int)(2 * _offset.Y / RowHeight);
                    double verticalOffset = lastIndex * RowHeight;
                    //SetVerticalOffset(verticalOffset);
                }
                _loaded = true;
            }
            return arrangeSize;
        }

        #region IScrollInfo implementation

        private void UpdateScrollInfo(Size availableSize)
        {
            int itemCount = InternalChildren.Count;
            bool viewportChanged = false;
            bool extentChanged = false;

            Size extent = CalculateExtent(availableSize, itemCount);
            if (extent != _extent)
            {
                _extent = extent;
                extentChanged = true;
            }

            if (availableSize != _viewport)
            {
                _viewport = availableSize;
                viewportChanged = true;
            }

            if ((extentChanged || viewportChanged) && _scrollOwner != null)
            {
                _offset.Y = CalculateVerticalOffset(VerticalOffset);
                _offset.X = CalculateHorizontalOffset(HorizontalOffset);
                _scrollOwner.InvalidateScrollInfo();

                Scroll(HorizontalOffset, VerticalOffset);
            }
        }

        private Size CalculateExtent(Size availableSize, int itemCount)
        {
            return new Size(availableSize.Width, RowHeight * itemCount);
        }


        public ScrollViewer ScrollOwner
        {
            get { return _scrollOwner; }
            set { _scrollOwner = value; }
        }

        public bool CanHorizontallyScroll
        {
            get { return _canHScroll; }
            set { _canHScroll = value; }
        }

        public bool CanVerticallyScroll
        {
            get { return _canVScroll; }
            set { _canVScroll = value; }
        }

        public double HorizontalOffset
        {
            get { return _offset.X; }
        }

        public double VerticalOffset
        {
            get { return _offset.Y; }
        }

        public double ExtentHeight
        {
            get { return _extent.Height; }
        }

        public double ExtentWidth
        {
            get { return _extent.Width; }
        }

        public double ViewportHeight
        {
            get { return _viewport.Height; }
        }

        public double ViewportWidth
        {
            get { return _viewport.Width; }
        }

        public void LineUp()
        {
            SetVerticalOffset(VerticalOffset - 10);
        }

        public void LineDown()
        {
            SetVerticalOffset(VerticalOffset + 10);
        }

        public void PageUp()
        {
            SetVerticalOffset(VerticalOffset - ViewportHeight);
        }

        public void PageDown()
        {
            SetVerticalOffset(VerticalOffset + ViewportHeight);
        }

        public void MouseWheelUp()
        {
            SetVerticalOffset(this.VerticalOffset - 10);
        }

        public void MouseWheelDown()
        {
            SetVerticalOffset(this.VerticalOffset + 10);
        }

        public void LineLeft()
        {
            SetHorizontalOffset(HorizontalOffset - 10);
        }

        public void LineRight()
        {
            SetHorizontalOffset(HorizontalOffset + 10);
        }

        public Rect MakeVisible(Visual visual, Rect rectangle)
        {
            return rectangle;
        }

        public void MouseWheelLeft()
        {
            SetHorizontalOffset(HorizontalOffset - 10);
        }

        public void MouseWheelRight()
        {
            SetHorizontalOffset(HorizontalOffset + 10);
        }

        public void PageLeft()
        {
            SetHorizontalOffset(HorizontalOffset - ViewportWidth);
        }

        public void PageRight()
        {
            SetHorizontalOffset(HorizontalOffset + ViewportWidth);
        }

        public void SetHorizontalOffset(double offset)
        {
            offset = CalculateHorizontalOffset(offset);

            _offset.X = offset;

            if (_scrollOwner != null)
                _scrollOwner.InvalidateScrollInfo();

            Scroll(offset, VerticalOffset);

            InvalidateMeasure();
        }

        public void SetVerticalOffset(double offset)
        {
            offset = CalculateVerticalOffset(offset);

            _offset.Y = offset;

            if (_scrollOwner != null)
                _scrollOwner.InvalidateScrollInfo();

            Scroll(HorizontalOffset, offset);
        }

        private double CalculateVerticalOffset(double offset)
        {
            if (offset < 0 || _viewport.Height >= _extent.Height)
            {
                offset = 0;
            }
            else
            {
                if (offset + _viewport.Height >= _extent.Height)
                {
                    offset = _extent.Height - _viewport.Height;
                    if (_loaded)
                    {
                        LazySlidesCount = 5;                        
                    }
                }
            }
            return offset;
        }

        private double CalculateHorizontalOffset(double offset)
        {
            if (offset < 0 || _viewport.Width >= _extent.Width)
            {
                offset = 0;
            }
            else
            {
                if (offset + _viewport.Width >= _extent.Width)
                {
                    offset = _extent.Width - _viewport.Width;
                }
            }
            return offset;
        }

        private void Scroll(double xOffset, double yOffset)
        {
            if (AnimateScroll)
            {
                DoubleAnimation anim = new DoubleAnimation(-yOffset, new Duration(TimeSpan.FromMilliseconds(100)));
                PropertyPath p = new PropertyPath("(0).(1)", RenderTransformProperty, TranslateTransform.YProperty);
                Storyboard.SetTargetProperty(anim, p);

                Storyboard sb = new Storyboard();
                sb.Children.Add(anim);
                EventHandler handler = null;
                handler = delegate
                {
                    sb.Completed -= handler;
                    sb.Remove(this);

                    _trans.X = -xOffset;
                    _trans.Y = -yOffset;
                };
                sb.Completed += handler;
                sb.Begin(this, true);
            }
            else
            {
                _trans.X = -xOffset;
                _trans.Y = -yOffset;
            }
        }

        private TranslateTransform _trans = new TranslateTransform();
        private ScrollViewer _scrollOwner;
        private bool _canHScroll = false;
        private bool _canVScroll = false;
        private Size _extent = new Size(0, 0);
        private Size _viewport = new Size(0, 0);
        private Point _offset;

        #endregion

    }
}
