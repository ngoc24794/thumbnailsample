﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ThumbnailSample.Controls
{
    public class ThumbnailPanel : Panel
    {
        public UIElement NumberedLabel
        {
            get { return (UIElement)GetValue(NumberedLabelProperty); }
            set { SetValue(NumberedLabelProperty, value); }
        }

        public static readonly DependencyProperty NumberedLabelProperty =
            DependencyProperty.Register("NumberedLabel", typeof(UIElement), typeof(ThumbnailPanel), new PropertyMetadata(null, UIElementAssignedCallback));

        public UIElement Thumbnail
        {
            get { return (UIElement)GetValue(ThumbnailProperty); }
            set { SetValue(ThumbnailProperty, value); }
        }

        public static readonly DependencyProperty ThumbnailProperty =
            DependencyProperty.Register("Thumbnail", typeof(UIElement), typeof(ThumbnailPanel), new PropertyMetadata(null, UIElementAssignedCallback));

        private static void UIElementAssignedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UIElement uIElement = e.NewValue as UIElement;
            if (VisualTreeHelper.GetParent(uIElement) == null)
            {
                (d as ThumbnailPanel).AddVisualChild(uIElement);
            }
        }

        protected override Size MeasureOverride(Size constraint)
        {
            if (double.IsInfinity(constraint.Width) || double.IsInfinity(constraint.Height))
            {
                return Size.Empty;
            }

            NumberedLabel?.Measure(constraint);
            Thumbnail?.Measure(constraint);
            double
                labelSize = NumberedLabel.DesiredSize.Height,
                thumbnailSize = constraint.Width * 9 / 16,
                height = labelSize + thumbnailSize;

            return new Size(constraint.Width, height);

        }

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            Size size = arrangeSize;

            double
                labelSize = NumberedLabel.DesiredSize.Height,
                thumbnailSize = size.Width * 9 / 16,
                height = labelSize + thumbnailSize;

            NumberedLabel?.Arrange(new Rect(0, 0, size.Width, labelSize));
            Thumbnail?.Arrange(new Rect(0, labelSize, size.Width, thumbnailSize));

            return arrangeSize;
        }

        protected override int VisualChildrenCount => 2;
        protected override Visual GetVisualChild(int index)
        {
            return index == 0 ? NumberedLabel : Thumbnail;
        }
    }
}
