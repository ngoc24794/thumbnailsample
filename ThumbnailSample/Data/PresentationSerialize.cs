﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using ThumbnailSample.Models;

namespace ThumbnailSample.Data
{
    public class PresentationSerialize : ISerialization<Presentation>
    {
        public Presentation Deserialize()
        {
            Presentation presentation = new Presentation();
            if (File.Exists("Presentation.xml"))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Presentation));
                using (FileStream fs = new FileStream("Presentation.xml", FileMode.Open))
                {
                    presentation = xmlSerializer.Deserialize(fs) as Presentation;
                }
            }
            return presentation;
        }

        public void Serialize(Presentation @object)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Presentation));
            using (FileStream fs = new FileStream("Presentation.xml", FileMode.Create))
            {
                xmlSerializer.Serialize(fs, @object);
            }
        }
    }
}
