﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ThumbnailSample.ViewModels;

namespace ThumbnailSample.Controls
{
    public class ShapeItemTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            IDataTemplateSelector dataTemplateSelector = item as IDataTemplateSelector;
            if (dataTemplateSelector == null)
            {
                return base.SelectTemplate(item, container);
            }
            if (Application.Current.TryFindResource(dataTemplateSelector.DataTemplateKey) is DataTemplate dataTemplate)
            {
                return dataTemplate;
            }
            return base.SelectTemplate(item, container);
        }
    }
}
