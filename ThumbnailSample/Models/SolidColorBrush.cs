﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbnailSample.Models
{
    [Serializable]
    public class SolidColorBrush : ModelBase
    {
        public Color Color { get; set; }

        public static implicit operator System.Windows.Media.SolidColorBrush(SolidColorBrush solidColorBrush)
        {
            return new System.Windows.Media.SolidColorBrush()
            {
                Color = solidColorBrush.Color
            };
        }
    }
}
