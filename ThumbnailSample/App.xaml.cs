﻿using ThumbnailSample.Views;
using Prism.Ioc;
using Prism.Modularity;
using System.Windows;
using ThumbnailSample.Data;
using ThumbnailSample.ViewModels;
using ThumbnailSample.Models;
using AutoMapper;
using Unity.Injection;
using Unity;
using System.Windows.Media;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace ThumbnailSample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<IMapping<SlideViewModel, Slide>, SlideMapping>("SlideMapping");
            containerRegistry.Register<IMapping<ShapeViewModel, Shape>, ShapeMapping>("ShapeMapping");
            containerRegistry.Register<ISerialization<Presentation>, PresentationSerialize>("PresentationSerialize");
        }
    }
}
