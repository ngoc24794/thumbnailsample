﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;
using ThumbnailSample.Data;
using ThumbnailSample.Events;
using ThumbnailSample.Models;
using Unity;

namespace ThumbnailSample.ViewModels
{
    public class SlideViewModel : BindableBase, ICloneable
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly IUnityContainer _container;

        public SlideViewModel(IEventAggregator eventAggregator, IUnityContainer container)
        {
            _eventAggregator = eventAggregator;
            _container = container;
            Id = Guid.NewGuid().ToString();
        }

        public string Id { get; set; }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                SetProperty(ref _isSelected, value);
                _eventAggregator.GetEvent<SlideSelectedEvent>().Publish(this);
            }
        }

        private string _title = "[No title]";

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private Brush _background = Brushes.White;

        public Brush Background
        {
            get { return _background; }
            set { SetProperty(ref _background, value); }
        }

        private ObservableCollection<ShapeViewModel> _shapeCollection;

        public ObservableCollection<ShapeViewModel> ShapeCollection
        {
            get { return _shapeCollection ?? (_shapeCollection = new ObservableCollection<ShapeViewModel>()); }
        }

        public object Clone()
        {
            SlideViewModel slideViewModel = _container.Resolve<SlideViewModel>();
            slideViewModel.IsSelected = IsSelected;
            return slideViewModel;
        }
    }

    public class SlideMapping : IMapping<SlideViewModel, Slide>
    {
        private readonly IUnityContainer _container;
        private readonly IMapping<ShapeViewModel, Shape> _shapeMapping;

        public SlideMapping(IUnityContainer container)
        {
            _container = container;
            _shapeMapping = _container.Resolve< IMapping<ShapeViewModel, Shape>>("ShapeMapping");
        }

        public SlideViewModel GetLeftObject(Slide @object)
        {
            SlideViewModel slideViewModel = _container.Resolve<SlideViewModel>();
            slideViewModel.Id = @object.Id;
            slideViewModel.Title = @object.Title;
            if (@object.Background != null)
            {
                Models.Color data = @object.Background.Color;
                System.Windows.Media.Color color = System.Windows.Media.Color.FromRgb(data.R, data.G, data.B);
                slideViewModel.Background = new System.Windows.Media.SolidColorBrush(color);
            }
            if (@object.Shapes?.Count > 0)
            {
                foreach (Shape shape in @object.Shapes)
                {
                    ShapeViewModel shapeViewModel = _shapeMapping.GetLeftObject(shape);
                    slideViewModel.ShapeCollection.Add(shapeViewModel);
                }
            }
            return slideViewModel;
        }

        public Slide GetRightObject(SlideViewModel @object)
        {
            Models.SolidColorBrush background = null;
            if (@object.Background is System.Windows.Media.SolidColorBrush solidColorBrush)
            {
                System.Windows.Media.Color color = solidColorBrush.Color;
                background = new Models.SolidColorBrush
                {
                    Color = new Models.Color()
                    {
                        R = color.R,
                        G = color.G,
                        B = color.B
                    }
                };
            }
            List<Shape> shapes = null;
            if (@object.ShapeCollection?.Count > 0)
            {
                shapes = new List<Shape>();
                foreach (ShapeViewModel shapeViewModel in @object.ShapeCollection)
                {
                    Shape shape = _shapeMapping.GetRightObject(shapeViewModel);
                    shapes.Add(shape);
                }
            }

            return new Slide()
            {
                Id = @object.Id,
                Title = @object.Title,
                Background = background,
                Shapes = shapes
            };
        }
    }
}
