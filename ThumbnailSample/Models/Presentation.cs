﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbnailSample.Models
{
    [Serializable]
    public class Presentation : ModelBase
    {
        public List<Slide> Slides { get; set; }
    }
}
