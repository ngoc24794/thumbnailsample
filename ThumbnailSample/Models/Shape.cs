﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbnailSample.Models
{
    [Serializable]
    public class Shape : ModelBase
    {
        public double Width { get; set; }
        public double Height { get; set; }
        public double Left { get; set; }
        public double Top { get; set; }
        public SolidColorBrush Fill { get; set; }
    }
}
