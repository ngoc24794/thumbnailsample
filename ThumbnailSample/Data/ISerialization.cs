﻿namespace ThumbnailSample.Data
{
    public interface ISerialization<T>
    {
        void Serialize(T @object);
        T Deserialize();
    }
}
