﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbnailSample.Data
{
    public interface IMapping<T1, T2>
    {
        T1 GetLeftObject(T2 @object);
        T2 GetRightObject(T1 @object);
    }
}
