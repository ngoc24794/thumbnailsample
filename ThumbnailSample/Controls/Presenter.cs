﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace ThumbnailSample.Controls
{
    public class Presenter : Panel, IScrollInfo
    {
        public double SlideWidth
        {
            get { return (double)GetValue(SlideWidthProperty); }
            set { SetValue(SlideWidthProperty, value); }
        }

        public static readonly DependencyProperty SlideWidthProperty =
            DependencyProperty.Register("SlideWidth", typeof(double), typeof(Presenter), new PropertyMetadata(0.0));

        public double SlideHeight
        {
            get { return (double)GetValue(SlideHeightProperty); }
            set { SetValue(SlideHeightProperty, value); }
        }

        public static readonly DependencyProperty SlideHeightProperty =
            DependencyProperty.Register("SlideHeight", typeof(double), typeof(Presenter), new PropertyMetadata(0.0));

        public UIElement UIElement
        {
            get { return (UIElement)GetValue(UIElementProperty); }
            set { SetValue(UIElementProperty, value); }
        }

        public static readonly DependencyProperty UIElementProperty =
            DependencyProperty.Register("UIElement", typeof(UIElement), typeof(Presenter), new PropertyMetadata(null, UIElementChangedCallback));

        public double ZoomScale
        {
            get { return (double)GetValue(ZoomScaleProperty); }
            set { SetValue(ZoomScaleProperty, value); }
        }

        public static readonly DependencyProperty ZoomScaleProperty =
            DependencyProperty.Register("ZoomScale", typeof(double), typeof(Presenter), new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsArrange));

        public bool IsPresent
        {
            get { return (bool)GetValue(IsPresentProperty); }
            set { SetValue(IsPresentProperty, value); }
        }

        public static readonly DependencyProperty IsPresentProperty =
            DependencyProperty.Register("IsPresent", typeof(bool), typeof(Presenter), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsArrange));
        
        private static void UIElementChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Presenter).Update();
        }

        private void Update()
        {
            Children.Clear();
            if (VisualTreeHelper.GetParent(UIElement) == null)
            {
                // Đăng ký EventHandlers
                UIElement.MouseLeftButtonDown += UIElement_MouseLeftButtonDown;
                UIElement.MouseLeftButtonUp += (s, m) =>
                {
                    UIElement.ReleaseMouseCapture();
                };
                UIElement.MouseMove += UIElement_MouseMove;

                // Thêm vào VisualTree
                Children.Add(UIElement);
            }
        }

        private Point _start = new Point();
        private void UIElement_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (UIElement.IsMouseCaptured) return;
            _start = e.GetPosition(this);
            UIElement.CaptureMouse();
        }

        private void UIElement_MouseMove(object sender, MouseEventArgs e)
        {
            if (!UIElement.IsMouseCaptured) return;
            if (Keyboard.IsKeyDown(Key.Space))
            {
                Point pointer = e.GetPosition(this);
                Vector vector = (Vector)pointer - (Vector)_start;
                _start = pointer;

                // Cuộn nội dung
                SetHorizontalOffset(_offset.X - vector.X);
                SetVerticalOffset(_offset.Y - vector.Y);
            }
        }

        protected override Size MeasureOverride(Size constraint)
        {
            if (constraint.Width == double.PositiveInfinity || constraint.Height == double.PositiveInfinity)
                return Size.Empty;
            UIElement?.Measure(constraint);
            return constraint;
        }

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            if (IsPresent)
            {
                Point origin = new Point((arrangeSize.Width - SlideWidth) / 2, (arrangeSize.Height - SlideHeight) / 2);
                double
                    x = origin.X,
                    y = origin.Y;
                UIElement.Arrange(new Rect(x, y, SlideWidth, SlideHeight));
                UpdateScrollInfo();
            }
            return arrangeSize;
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                UpdateScrollInfo();
                Matrix matrix = UIElement.RenderTransform.Value;
                Point pointer = e.MouseDevice.GetPosition(UIElement);
                double factor = e.Delta > 0 ? 1.15 : 1 / 1.15;
                matrix.ScaleAtPrepend(factor, factor, pointer.X, pointer.Y);
                UIElement.RenderTransform = new MatrixTransform(matrix);
            }
        }

        protected override int VisualChildrenCount => 1;
        protected override Visual GetVisualChild(int index)
        {
            return UIElement;
        }

        #region IScrollInfo implementation

        private void UpdateScrollInfo()
        {
            Size viewport = RenderSize, extent = CalculateExtentSize();
            bool viewportChanged = false;
            bool extentChanged = false;

            if (extent != _extent)
            {
                _extent = extent;
                extentChanged = true;
            }

            if (viewport != _viewport)
            {
                _viewport = viewport;
                viewportChanged = true;
            }

            if ((extentChanged || viewportChanged) && ScrollOwner != null)
            {
                _offsetXMax = Math.Max(0, _extent.Width - _viewport.Width);
                _offsetYMax = Math.Max(0, _extent.Height - _viewport.Height);
                SetVerticalOffset(VerticalOffset);
                SetHorizontalOffset(HorizontalOffset);
                InvalidateScrollInfo();
            }
        }

        private Size CalculateExtentSize()
        {
            Rect bounds = GetBounds();
            Size extent = new Size(bounds.Width * 3, bounds.Height * 3);
            return extent;
        }

        private Rect GetBounds()
        {
            return UIElement.TransformToVisual(this).TransformBounds(LayoutInformation.GetLayoutSlot((UIElement as FrameworkElement)));
        }

        private void InvalidateScrollInfo()
        {
            if (IsPresent)
            {
                ScrollOwner.InvalidateScrollInfo();
            }
        }

        public ScrollViewer ScrollOwner { get; set; }

        public bool CanHorizontallyScroll { get; set; } = false;

        public bool CanVerticallyScroll { get; set; } = false;

        public double HorizontalOffset
        {
            get { return _offset.X; }
        }

        public double VerticalOffset
        {
            get { return _offset.Y; }
        }

        public double ExtentHeight
        {
            get { return _extent.Height; }
        }

        public double ExtentWidth
        {
            get { return _extent.Width; }
        }

        public double ViewportHeight
        {
            get { return _viewport.Height; }
        }

        public double ViewportWidth
        {
            get { return _viewport.Width; }
        }

        public void LineUp()
        {
            SetVerticalOffset(VerticalOffset - 20);
        }

        public void LineDown()
        {
            SetVerticalOffset(VerticalOffset + 20);
        }

        public void PageUp()
        {
            SetVerticalOffset(VerticalOffset - ViewportHeight);
        }

        public void PageDown()
        {
            SetVerticalOffset(VerticalOffset + ViewportHeight);
        }

        public void MouseWheelUp()
        {
            if ((Keyboard.Modifiers & ModifierKeys.Control) != ModifierKeys.Control)
            {
                SetVerticalOffset(VerticalOffset - 20);
            }
        }

        public void MouseWheelDown()
        {
            if ((Keyboard.Modifiers & ModifierKeys.Control) != ModifierKeys.Control)
            {
                SetVerticalOffset(VerticalOffset + 20);
            }
        }

        public void LineLeft()
        {
            SetHorizontalOffset(HorizontalOffset - 20);
        }

        public void LineRight()
        {
            SetHorizontalOffset(HorizontalOffset + 20);
        }

        public Rect MakeVisible(Visual visual, Rect rectangle)
        {
            return rectangle;
        }

        public void MouseWheelLeft()
        {
            SetHorizontalOffset(HorizontalOffset - 20);
        }

        public void MouseWheelRight()
        {
            SetHorizontalOffset(HorizontalOffset + 20);
        }

        public void PageLeft()
        {
            SetHorizontalOffset(HorizontalOffset - ViewportWidth);
        }

        public void PageRight()
        {
            SetHorizontalOffset(HorizontalOffset + ViewportWidth);
        }

        public void SetHorizontalOffset(double offset)
        {
            // Xác thực
            offset = ValidateHorizontalOffset(offset);

            // Di chuyển nội dung
            double deltaX = _offset.X - offset;
            Move(deltaX, 0);

            // Đặt vị trí thanh cuộn
            _offset.X = offset;
            ScrollOwner?.InvalidateScrollInfo();

        }

        public void SetVerticalOffset(double offset)
        {
            // Xác thực
            offset = ValidateVerticalOffset(offset);

            // Di chuyển nội dung
            double deltaY = _offset.Y - offset;
            Move(0, deltaY);

            // Đặt vị trí thanh cuộn
            _offset.Y = offset;
            ScrollOwner?.InvalidateScrollInfo();
        }

        private double ValidateVerticalOffset(double offset)
        {
            offset = Math.Max(0, Math.Min(_offsetYMax, offset));
            //---------------------------------------------------------------------------
            // -_offsetYMax <= offset <= _offsetYMax
            //---------------------------------------------------------------------------
            return offset;
        }

        private double ValidateHorizontalOffset(double offset)
        {
            offset = Math.Max(0, Math.Min(_offsetXMax, offset));
            //---------------------------------------------------------------------------
            // -_offsetYMax <= offset <= _offsetYMax
            //---------------------------------------------------------------------------
            return offset;
        }

        private void Move(double deltaX, double deltaY)
        {
            Matrix matrix = UIElement.RenderTransform.Value;
            matrix.OffsetX += deltaX;
            matrix.OffsetY += deltaY;
            UIElement.RenderTransform = new MatrixTransform(matrix);
        }

        private Size _extent = new Size(0, 0);
        private Size _viewport = new Size(0, 0);
        private Point _offset;
        private double _offsetYMax, _offsetXMax;

        #endregion

    }
}
