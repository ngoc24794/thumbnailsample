﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ThumbnailSample.ViewModels;

namespace ThumbnailSample.Controls
{
    public class ContainerStyleSelector : StyleSelector
    {
        public override Style SelectStyle(object item, DependencyObject container)
        {
            IContainerTemplateSelector containerTemplateSelector = item as IContainerTemplateSelector;
            if (containerTemplateSelector == null)
            {
                return base.SelectStyle(item, container);
            }
            if (Application.Current.TryFindResource(containerTemplateSelector.ContainerTemplateKey) is Style style)
            {
                return style;
            }
            return base.SelectStyle(item, container);
        }
    }
}
