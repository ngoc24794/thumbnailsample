﻿using System;
using System.Globalization;
using System.Windows.Data;
using ThumbnailSample.ViewModels;

namespace ThumbnailSample.Converters
{
    [ValueConversion(typeof(SlideViewModel), typeof(bool))]
    public class PresenterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
