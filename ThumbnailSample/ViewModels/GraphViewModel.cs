﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbnailSample.ViewModels
{
    public class GraphViewModel : ShapeViewModel, IDataTemplateSelector, IContainerTemplateSelector
    {
        public override string DataTemplateKey => "GraphDataTemplate";
        public override string ContainerTemplateKey => "GraphContainerStyle";
    }
}
