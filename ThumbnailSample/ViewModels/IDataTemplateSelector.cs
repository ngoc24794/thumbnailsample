﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbnailSample.ViewModels
{
    public interface IDataTemplateSelector
    {
        string DataTemplateKey { get; }
    }
}
