﻿using AutoMapper;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using ThumbnailSample.Data;
using ThumbnailSample.Events;
using ThumbnailSample.Models;
using Unity;

namespace ThumbnailSample.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly IUnityContainer _container;
        private readonly IMapping<SlideViewModel, Slide> _slideMapping;
        public MainWindowViewModel(IUnityContainer container, IEventAggregator eventAggregator)
        {
            _container = container;
            eventAggregator.GetEvent<SlideSelectedEvent>().Subscribe((s) =>
            {
                SelectedItemCount = SlideCollection.Count(slide => slide.IsSelected);
            });
            _slideMapping = _container.Resolve<IMapping<SlideViewModel, Slide>>("SlideMapping");
        }

        private string _title = "WPF Application";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _selectedItemCountLabel = "Nothing selected";

        public string SelectedItemCountLabel
        {
            get { return _selectedItemCountLabel; }
            set { SetProperty(ref _selectedItemCountLabel, value); }
        }

        private int _selectedItemCount;

        public int SelectedItemCount
        {
            get { return _selectedItemCount; }
            set
            {
                if (value != _selectedItemCount)
                {
                    SelectedItemCountLabel = $"{value} seleted slide";
                }
                _selectedItemCount = value;
            }
        }

        private SlideViewModel _selectedSlide;

        public SlideViewModel SelectedSlide
        {
            get { return _selectedSlide; }
            set { SetProperty(ref _selectedSlide, value); }
        }

        private ObservableCollection<SlideViewModel> _slideCollection;

        public ObservableCollection<SlideViewModel> SlideCollection
        {
            get { return _slideCollection ?? (_slideCollection = new ObservableCollection<SlideViewModel>()); }
            set { _slideCollection = value; }
        }

        private double _zoomScale = 1.0;

        public double ZoomScale
        {
            get { return _zoomScale; }
            set { SetProperty(ref _zoomScale, value); }
        }

        private double _slideWidth = 1600;

        public double SlideWidth
        {
            get { return _slideWidth; }
            set { SetProperty(ref _slideWidth, value); }
        }

        private double _slideHeight = 900;

        public double SlideHeight
        {
            get { return _slideHeight; }
            set { SetProperty(ref _slideHeight, value); }
        }

        private double _thumbnailSize;

        public double ThumbnailSize
        {
            get { return _thumbnailSize; }
            set { _thumbnailSize = value; }
        }

        private ICommand _insertSlideCommand;

        public ICommand InsertSlideCommand
        {
            get { return _insertSlideCommand ?? (_insertSlideCommand = new DelegateCommand(InsertSlide)); }
        }

        private void InsertSlide()
        {
            SlideCollection.Add(_container.Resolve<SlideViewModel>());
        }

        private ICommand _duplicateSlideCommand;

        public ICommand DuplicateSlideCommand
        {
            get { return _duplicateSlideCommand ?? (_duplicateSlideCommand = new DelegateCommand(DuplicateSlide)); }
        }

        private void DuplicateSlide()
        {
            var selectedSlides = SlideCollection.Where(s => s.IsSelected).ToList();
            if (selectedSlides != null)
            {
                List<SlideViewModel> slides = new List<SlideViewModel>();
                foreach (SlideViewModel slide in selectedSlides)
                {
                    SlideViewModel clonedSlide = slide.Clone() as SlideViewModel;
                    clonedSlide.Id = Guid.NewGuid().ToString();
                    clonedSlide.IsSelected = true;
                    slides.Add(clonedSlide);
                }
                AddRange(slides);
            }
        }

        private ICommand _copySlideCommand;

        public ICommand CopySlideCommand
        {
            get { return _copySlideCommand ?? (_copySlideCommand = new DelegateCommand(CopySlide)); }
        }

        private void CopySlide()
        {
            var selectedSlides = SlideCollection.Where(s => s.IsSelected).ToList();
            if (selectedSlides != null)
            {
                List<Slide> slides = new List<Slide>();
                for (int i = 0; i < selectedSlides.Count; i++)
                {
                    SlideViewModel slide = selectedSlides[i];
                    Slide data = _slideMapping.GetRightObject(slide);
                    slides.Add(data);
                }
                Clipboard.SetData("CLIPBOARD.CopedSlides", slides);
            }
        }

        private ICommand _pasteSlideCommand;

        public ICommand PasteSlideCommand
        {
            get { return _pasteSlideCommand ?? (_pasteSlideCommand = new DelegateCommand(PasteSlide, () => Clipboard.ContainsData("CLIPBOARD.CopedSlides"))); }
        }

        private void PasteSlide()
        {
            List<Slide> slides = Clipboard.GetData("CLIPBOARD.CopedSlides") as List<Slide>;
            if (slides?.Count > 0)
            {
                List<SlideViewModel> addedSlides = new List<SlideViewModel>();
                foreach (Slide slide in slides)
                {
                    SlideViewModel slideViewModel = _slideMapping.GetLeftObject(slide);
                    slideViewModel.Id = Guid.NewGuid().ToString();
                    addedSlides.Add(slideViewModel);
                    slideViewModel.IsSelected = true;
                }
                AddRange(addedSlides);
            }
        }

        private void AddRange(List<SlideViewModel> addedSlides)
        {
            int selectdItemCount = SelectedItemCount;
            SlideViewModel selectedLastSlide = SlideCollection.LastOrDefault(s => s.IsSelected);
            SetIsSelected(SlideCollection.ToList(), false);
            if (selectdItemCount > 0)
            {
                int lastIndex = SlideCollection.IndexOf(selectedLastSlide);
                foreach (SlideViewModel addedSlide in addedSlides)
                {
                    SlideCollection.Insert(lastIndex + 1, addedSlide);
                    lastIndex++;
                }
            }
            else
            {
                SlideCollection.AddRange(addedSlides);
            }
            SetIsSelected(addedSlides, true);
        }

        private static void SetIsSelected(List<SlideViewModel> slides, bool isSelected)
        {
            foreach (SlideViewModel slide in slides)
            {
                slide.IsSelected = isSelected;
            }
        }

        private ICommand _deleteSlideCommand;

        public ICommand DeleteSlideCommand
        {
            get { return _deleteSlideCommand ?? (_deleteSlideCommand = new DelegateCommand(DeleteSlide)); }
        }

        private void DeleteSlide()
        {
            var selectedSlides = SlideCollection.Where(s => s.IsSelected).ToList();
            if (selectedSlides != null)
            {
                for (int i = 0; i < selectedSlides.Count; i++)
                {
                    SlideViewModel slideViewModel = selectedSlides[i];
                    SlideCollection.Remove(slideViewModel);
                }
            }
        }

        private ICommand _insertShapeCommand;

        public ICommand InsertShapeCommand
        {
            get { return _insertShapeCommand ?? (_insertShapeCommand = new DelegateCommand(InsertShape, () => SelectedSlide != null).ObservesProperty(() => SelectedSlide)); }
        }

        private void InsertShape()
        {
            for (int i = 0; i < 100; i++)
            {
                ShapeViewModel shapeViewModel = _container.Resolve<ShapeViewModel>();
                shapeViewModel.Width = 200;
                shapeViewModel.Height = 120;
                shapeViewModel.Left = 100;
                shapeViewModel.Top = 100;
                shapeViewModel.Fill = Brushes.Blue;
                SelectedSlide.ShapeCollection.Add(shapeViewModel);
            }
            GraphViewModel graphViewModel = _container.Resolve<GraphViewModel>();
            graphViewModel.Width = 200;
            graphViewModel.Height = 120;
            graphViewModel.Left = 200;
            graphViewModel.Top = 100;
            graphViewModel.Fill = Brushes.Green;
            SelectedSlide.ShapeCollection.Add(graphViewModel);
        }

        private ICommand _savePresentationCommand;

        public ICommand SavePresentationCommand
        {
            get { return _savePresentationCommand ?? (_savePresentationCommand = new DelegateCommand(SavePresentation)); }
        }

        private void SavePresentation()
        {
            ISerialization<Presentation> serialization = _container.Resolve<ISerialization<Presentation>>("PresentationSerialize");
            Presentation presentation = new Presentation();
            List<Slide> slides = new List<Slide>();
            foreach (SlideViewModel slideViewModel in SlideCollection)
            {
                Slide slide = _slideMapping.GetRightObject(slideViewModel);
                slides.Add(slide);
            }
            presentation.Slides = slides;
            serialization.Serialize(presentation);
        }

        private Queue<SlideViewModel> _delayedSlideCollection;

        private ICommand _openPresentationCommand;

        public ICommand OpenPresentationCommand
        {
            get { return _openPresentationCommand ?? (_openPresentationCommand = new DelegateCommand(OpenPresentation)); }
        }

        private void OpenPresentation()
        {
            ISerialization<Presentation> serialization = _container.Resolve<ISerialization<Presentation>>("PresentationSerialize");
            Presentation presentation = serialization.Deserialize();
            _delayedSlideCollection = new Queue<SlideViewModel>();
            BackgroundWorker backgroundWorker = new BackgroundWorker()
            {
                WorkerReportsProgress = true
            };
            backgroundWorker.DoWork += (s, e) =>
            {
                int slidesCount = presentation.Slides.Count;
                for (int i = 0; i < slidesCount; i++)
                {
                    SlideViewModel slideViewModel = _slideMapping.GetLeftObject(presentation.Slides[i]);
                    slideViewModel.Background.Freeze();
                    foreach (ShapeViewModel shapeViewModel in slideViewModel.ShapeCollection)
                    {
                        shapeViewModel.Fill.Freeze();
                    }
                    _delayedSlideCollection.Enqueue(slideViewModel);
                    int count = i + 1;
                    backgroundWorker.ReportProgress(count / slidesCount * 100, count);
                }
            };
            backgroundWorker.ProgressChanged += (s, e) =>
            {
                SelectedItemCountLabel = $"{e.UserState}/{presentation.Slides.Count} slides";
            };
            backgroundWorker.RunWorkerCompleted += (s, e) =>
            {
                LazyLoadSlideCollection();
            };
            backgroundWorker.RunWorkerAsync();
        }

        private void LazyLoadSlideCollection(int count = 30)
        {
            count = Math.Min(count, _delayedSlideCollection.Count);
            for (int i = 0; i < count; i++)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action)(() =>
                {
                    if (_delayedSlideCollection.Count > 0)
                    {
                        SlideViewModel slideViewModel = _delayedSlideCollection.Dequeue();
                        SlideCollection.Add(slideViewModel); 
                    }
                }));
            }
        }

        private int _lazySlidesCount;

        public int LazySlidesCount
        {
            get { return _lazySlidesCount; }
            set
            {
                _lazySlidesCount = value;
                LazyLoadSlideCollection(value);
            }
        }
    }
}
