﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbnailSample.Models
{
    [Serializable]
    public abstract class ModelBase
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
}
