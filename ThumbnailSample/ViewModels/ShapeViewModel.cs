﻿using AutoMapper;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using ThumbnailSample.Data;
using ThumbnailSample.Models;

namespace ThumbnailSample.ViewModels
{
    public class ShapeViewModel : BindableBase, IDataTemplateSelector, IContainerTemplateSelector
    {
        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref _isSelected, value); }
        }

        private double _width;

        public double Width
        {
            get { return _width; }
            set { SetProperty(ref _width, value); }
        }

        private double _height;

        public double Height
        {
            get { return _height; }
            set { SetProperty(ref _height, value); }
        }

        private double _left;

        public double Left
        {
            get { return _left; }
            set { SetProperty(ref _left, value); }
        }

        private double _top;

        public double Top
        {
            get { return _top; }
            set { SetProperty(ref _top, value); }
        }

        private Brush _fill;

        public Brush Fill
        {
            get { return _fill; }
            set { SetProperty(ref _fill, value); }
        }

        public virtual string DataTemplateKey => "ShapeDataTemplate";

        public virtual string ContainerTemplateKey => "ShapeContainerStyle";
    }

    public class ShapeMapping : IMapping<ShapeViewModel, Shape>
    {
        public ShapeViewModel GetLeftObject(Shape @object)
        {
            ShapeViewModel shapeViewModel = new ShapeViewModel()
            {
                Width = @object.Width,
                Height = @object.Height,
                Left = @object.Left,
                Top = @object.Top,
                Fill = @object.Fill
            };
            
            return shapeViewModel;
        }

        public Shape GetRightObject(ShapeViewModel @object)
        {
            Models.SolidColorBrush fill = null;
            if (@object.Fill is System.Windows.Media.SolidColorBrush solidColorBrush)
            {
                System.Windows.Media.Color color = solidColorBrush.Color;
                fill = new Models.SolidColorBrush()
                {
                    Color = new Models.Color()
                    {
                        R = color.R,
                        G = color.G,
                        B = color.B
                    }
                };
            }
            Shape shape = new Shape()
            {
                Width = @object.Width,
                Height = @object.Height,
                Left = @object.Left,
                Top = @object.Top,
                Fill = fill
            };

            return shape;
        }
    }
}
